# README #

Требования
Проект должен быть создан с помощью Angular CLI
Создать навигацию между страницами
Страницы для отображения списка, редактирования и удаления
    Projects
    Project Tasks
    Team
    Users
Страницы для редактирования должны использовать Forms
Разбить всю логику на логические модули (projects, task...) и потом использовать их в root модуле
Использовать Guard для того, чтобы предупреждать пользователя, что у него есть несохраненные изменения
Данные загружать в сервисах (использовать библиотеку RxJS)
Создать свои директивы для того, чтобы визуально отличать статус Task
Создать Pipe для даты в формате “12 вересня 2021”