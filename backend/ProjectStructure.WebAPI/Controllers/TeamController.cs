﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetAllTeams()
        {
            return Ok(await _teamService.GetAllTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeam(int id)
        {
            var team = await _teamService.GetTeam(id);

            if (team == null)
            {
                return NotFound();
            }

            return Ok(team);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] NewTeamDTO newTeam)
        {
            return Ok(await _teamService.CreateTeam(newTeam));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TeamDTO>> UpdateTeam(int id, [FromBody] TeamDTO team)
        {
            if (id != team.Id)
            {
                return BadRequest();
            }

            var teamToUpdate = await _teamService.GetTeam(id);

            if (teamToUpdate == null)
            {
                return NotFound();
            }

            return Ok(await _teamService.UpdateTeam(team));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTeam(int id)
        {
            var teamToDelete = await _teamService.GetTeam(id);

            if (teamToDelete == null)
            {
                return NotFound();
            }

            await _teamService.DeleteTeam(id);
            return Ok();
        }
    }
}
