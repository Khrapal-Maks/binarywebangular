﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : BaseService, IUsersService
    {
        private readonly IUnitOfWork _database;

        public UsersService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            var users = await _database.GetRepositoryUsers.GetAll();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUser(int id)
        {
            var user = await _database.GetRepositoryUsers.Get(id);

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> CreateUser(NewUserDTO newUser)
        {
            var userEntity = _mapper.Map<User>(newUser);

            var createToUser = await _database.GetRepositoryUsers.Create(userEntity);
            await _database.Save();

            var createdUser = await _database.GetRepositoryUsers.Get(createToUser.Id);

            return _mapper.Map<UserDTO>(createdUser);
        }

        public async Task<UserDTO> UpdateUser(UserDTO user)
        {
            var updateUser = _mapper.Map<User>(user);

            await _database.GetRepositoryUsers.Update(user.Id, updateUser);
            await _database.Save();

            var updatedUser = await _database.GetRepositoryUsers.Get(user.Id);

            return _mapper.Map<UserDTO>(updatedUser);
        }

        public async Task DeleteUser(int id)
        {
            await _database.GetRepositoryUsers.Delete(id);
            await _database.Save();
        }
    }
}
