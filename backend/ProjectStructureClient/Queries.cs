﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectStructureClient
{
    public class Queries
    {
        private TaskCompletionSource<Task<int>> _completionSource;

        private Timer _timer;

        public List<Task> Tasks { get; private set; }

        public Queries(List<Task> tasks)
        {
            Tasks = tasks;
        }

        public Task<Task<int>> MarkRandomTaskWithDelay(int interval)
        {            
            _timer = new Timer(interval)
            {
                AutoReset = false
            };
            _timer.Elapsed += Marking;
            _timer.Start();

            _completionSource = new();

            return _completionSource.Task;
        }

        private void Marking(object o, ElapsedEventArgs e)
        {
            try
            {
                Random random = new();

                var task = Tasks.OrderBy(x => random.Next()).Where(x => x.Status != TaskStatus.RanToCompletion).First();
                task.Start();
                Task<int> t = Task.FromResult(task.Id);

                _completionSource.SetResult(t);
                _timer.Elapsed -= Marking;
            }
            catch (Exception ex)
            {
                _completionSource.SetException(ex);
            }
        }
    }
}
