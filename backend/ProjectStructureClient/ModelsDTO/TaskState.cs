﻿namespace ProjectStructureClient.ModelsDTO
{
    public enum TaskState
    {
        IsCreate,

        IsProgress,

        IsTesting,

        IsDone
    }
}
