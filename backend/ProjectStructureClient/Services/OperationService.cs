﻿using ProjectStructureClient.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructureClient.Services
{
    public class OperationService
    {
        private readonly ReportsWebService _webService;
        private Queries _queries;
        public List<Task> Tasks { get; }

        public OperationService()
        {
            _webService = new();

            Tasks = new()
            {
                new Task(async () => await GetAllUsersOldestThanTenYears()),
                new Task(async () => await SortAllUsersFirstNameAndSortTaskOnName()),
                new Task(async () => await GetStructAllProjects())
            };
        }

        public async Task GetAllTasksInProjectsByAuthor()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 1: Example author id 77");
            int authorId = ValidationId();         
            var taskHttp = _webService.GetAllTasksInProjectsByAuthorFromApi(authorId);
            taskHttp.GetAwaiter().GetResult();

            List<Tuple<ProjectDTO, int>> projectsTasks = await taskHttp;
            Console.Clear();
            if (projectsTasks.Count > 0)
            {
                Dictionary<ProjectDTO, int> dict = new();
                foreach (Tuple<ProjectDTO, int> item in projectsTasks)
                {
                    dict.Add(item.Item1, item.Item2);
                }

                Console.WriteLine("{0,-10} | {1,-55} | {2,-40}", "Project id", "Name project", "Count all tasks in project");
                foreach (KeyValuePair<ProjectDTO, int> item in dict)
                {
                    Console.WriteLine("{0,-10} | {1,-55} | {2,-40}", item.Key.Id, item.Key.Name, item.Value);
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task GetAllTasksOnPerformer()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 2: Example performer id 73");
            int performerId = ValidationId();
            var taskHttp = _webService.GetAllTasksOnPerformerFromApi(performerId);
            taskHttp.GetAwaiter().GetResult();

            List <TasksDTO> tasks = await taskHttp;

            Console.Clear();
            if (tasks.Count > 0)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"Task id: {task.Id}\ttask name: {task.Name}\tperformer id: {task.PerformerId} etc.");
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task GetAllTasksThatFinished()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 3: Example performer id 20 = true");
            int performerId = ValidationId();
            var taskHttp = _webService.GetAllTasksThatFinishedFromApi(performerId);
            taskHttp.GetAwaiter().GetResult();

            List<Tuple<int, string>> tasks = await taskHttp;

            Console.Clear();
            if (tasks.Count > 0)
            {
                Console.WriteLine("{0,-12} | {1,-8}", "Task id", "Task name");
                foreach (Tuple<int, string> item in tasks)
                {
                    Console.WriteLine("{0,-12} | {1,-8} ", item.Item1, item.Item2);
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task GetAllUsersOldestThanTenYears()
        {
            var taskHttp = _webService.GetAllUsersOldestThanTenYearsFromApi();
            taskHttp.GetAwaiter().GetResult();

            List<Tuple<int, string, List<UserDTO>>> usersTeams = await taskHttp;

            Console.Clear();
            if (usersTeams.Count > 0)
            {
                Console.WriteLine("Task 4");
                foreach (Tuple<int, string, List<UserDTO>> item in usersTeams)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"\nTeam id: {item.Item1}\tTeam name: {item.Item2}");
                    foreach (UserDTO user in item.Item3)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine($"Date registration: {user.RegisteredAt}\tUser id: {user.Id}\tUser name: {user.FirstName + " " + user.LastName}");
                    }
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task SortAllUsersFirstNameAndSortTaskOnName()
        {
            var taskHttp = _webService.SortAllUsersFirstNameAndSortTaskOnNameFromApi();
            taskHttp.GetAwaiter().GetResult();

            List<Tuple<string, List<TasksDTO>>> allUsersWithTheirTasks = await taskHttp;

            Console.Clear();
            if (allUsersWithTheirTasks.Count > 0)
            {
                Console.WriteLine("Task 5");
                foreach (Tuple<string, List<TasksDTO>> user in allUsersWithTheirTasks)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"User name: {user.Item1}");
                    Console.ForegroundColor = ConsoleColor.White;
                    foreach (TasksDTO task in user.Item2)
                    {
                        Console.WriteLine($"Task Id: {task.Id}\ttask name: {task.Name}");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task GetStructUserById()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 6: example user id 73");
            int id = ValidationId();
            var taskHttp = _webService.GetStructUserByIdFromApi(id);
            taskHttp.GetAwaiter().GetResult();

            UserInfoDTO? userInfo = await taskHttp;

            Console.Clear();

            if (userInfo == null)
            {
                Console.WriteLine("There is no information on the specified ID.");
            }
            else
            {
                List<UserInfoDTO> userInfos = new();
                userInfos.Add((UserInfoDTO)userInfo);

                if (userInfos.Count > 0)
                {
                    foreach (UserInfoDTO user in userInfos)
                    {
                        Console.WriteLine($"User id: {user.User.Id}\tUser name: {user.User.FirstName + " " + user.User.LastName} etc.");
                        Console.WriteLine($"Project id: {(user.Project == null ? 0 : user.Project.Id)}\tProject name: {user.Project?.Name} etc.");
                        Console.WriteLine($"Count all tasks in project: {user.CountTasksInProject}");
                        Console.WriteLine($"Count all tasks user in work: {user.CountTasksInWork}");
                        Console.WriteLine($"The longest task in work: task id: {user.Tasks.Id}\tperformer id: {user.Tasks.PerformerId}\tproject id: {user.Tasks.ProjectId} etc.");
                    }
                }
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task GetStructAllProjects()
        {
            var taskHttp = _webService.GetStructAllProjectsFromApi();
            taskHttp.GetAwaiter().GetResult();

            List <ProjectInfoDTO> projectInfo = await taskHttp;

            Console.Clear();
            if (projectInfo.Count > 0)
            {
                Console.WriteLine("Task 7");
                foreach (ProjectInfoDTO project in projectInfo)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Project id: {project.Project.Id}\tProject name: {project.Project.Name}");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"The longest task description in project: id {project.TaskLongDescription.Id}\ttext: {project.TaskLongDescription.Name}");
                    Console.WriteLine($"The shortest task name in project:       id {project.TasksShortName.Id}\ttext: {project.TasksShortName.Name}");
                    if (project.CountUsersInTeamProject == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    Console.WriteLine($"Count users in team: {project.CountUsersInTeamProject}\n");
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async Task GetMarkedTaskId()
        {
            _queries = new(Tasks);

            Console.Clear();
            Console.WriteLine("GetTaskCompletionSource started");
            var markedTaskId = _queries.MarkRandomTaskWithDelay(1000);
            Console.WriteLine("Write something here....");

            Console.ReadLine();

            Console.WriteLine($"\nTask finished with ID {await markedTaskId.Result}.\n");
            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        private static int ValidationId()
        {
            int id;

            Console.WriteLine("\t\t\t\t\tEnter number ID:");
            Console.SetCursorPosition(56, 10);
            while (!int.TryParse(Console.ReadLine(), out id))
            {
                Error();
            }
            return id;
        }

        private static void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\t\t\t\t\tInput error, check if the input is correct!\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t\t\tEnter number ID:");
            Console.SetCursorPosition(56, 10);
        }
    }
}
