﻿using Newtonsoft.Json;
using ProjectStructureClient.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProjectStructureClient.Services
{
    public class ReportsWebService
    {
        private const string API_PATH = "https://localhost:44322/api/Reports";

        private readonly HttpClient _client = new();        

        public ReportsWebService()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }       

        public async Task<List<Tuple<ProjectDTO, int>>> GetAllTasksInProjectsByAuthorFromApi(int id)
        { 
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllTasksInProjectsByAuthor/{id}");
            response.EnsureSuccessStatusCode();
          
            return JsonConvert.DeserializeObject<List<Tuple<ProjectDTO, int>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<TasksDTO>> GetAllTasksOnPerformerFromApi(int id)
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllTasksOnPerformer/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<TasksDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<int, string>>> GetAllTasksThatFinishedFromApi(int id)
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllTasksThatFinished/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<int, string>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<int, string, List<UserDTO>>>> GetAllUsersOldestThanTenYearsFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllUsersOldestThanTenYears");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<int, string, List<UserDTO>>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<string, List<TasksDTO>>>> SortAllUsersFirstNameAndSortTaskOnNameFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/SortAllUsersFirstNameAndSortTaskOnName");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<string, List<TasksDTO>>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<UserInfoDTO?> GetStructUserByIdFromApi(int id)
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetStructUserById/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserInfoDTO?>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<ProjectInfoDTO>> GetStructAllProjectsFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetStructAllProjects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<ProjectInfoDTO>>(await response.Content.ReadAsStringAsync());
        }
    }
}

