import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project-app';
  opened:boolean = false;

  ToggleSideBar() {
    this.opened = !this.opened;
  }

}
