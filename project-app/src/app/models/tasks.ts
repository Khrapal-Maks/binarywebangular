import { TaskStateEnum } from "./task-state";

export interface Tasks {
    id: number;
    projectId: number;
    performerId: number;
    name: string;
    description: string;
    state: TaskStateEnum,
    createdAt: Date;
    finishedAt: Date | null;
}