export enum TaskStateEnum {
    IsCreated = 0,
    IsProgress = 1,
    IsTesting = 2,
    IsDone = 3
}