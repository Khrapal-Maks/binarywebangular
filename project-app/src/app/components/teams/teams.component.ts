import { TeamService } from './../../services/team.service';
import { Team } from './../../models/team';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css'],
})
export class TeamsComponent implements OnInit {
  teams: Team[] = [];
  displayedColumns: string[] = [
    'No. team',
    'Name team',
    'Created at',
    'Update',
    'Delete',
  ];

  dataSource = new MatTableDataSource<Team>();

  @ViewChild(MatPaginator) set paginator(p: MatPaginator) {
    this.dataSource.paginator = p;
  }

  constructor(private _teamService: TeamService, private _router: Router) {}

  ngOnInit(): void {
    this.GetTeams();
    this.dataSource.paginator = this.paginator;
  }

  private GetTeams() {
    this._teamService.GetTeams().subscribe((resp) => {
      if (resp.body != null && resp.status == 200) {
        this.teams = this.dataSource.data = resp.body;
      }
    });
  }

  public redirectToUpdate(id: number) {
    this._router.navigate(['edit-team/', id]);
  }

  public redirectToDelete(team: Team) {
    var res = confirm('Are you sure you want to delete?');

    if (team && res) {
      this._teamService.DeleteTeam(team.id).subscribe((resp) => {
        if (resp.status == 200) {
          const indexDelete = this.teams.indexOf(team);
          this.teams.splice(indexDelete, 1);

          this.dataSource.data = [...this.teams];
        }
      });
    }
  }
}
