import { TeamService } from './../../services/team.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Team } from 'src/app/models/team';
import { FormatDatePipe } from 'src/app/pipe/format-date.pipe';

@Component({
  selector: 'app-team-editor',
  templateUrl: './team-editor.component.html',
  styleUrls: ['./team-editor.component.css'],
})
export class TeamEditorComponent implements OnInit {
  teamForm = this._taskForm.group({
    id: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    name: new FormControl(''),
    createdAt: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
  });

  team!: Team;
  oldTeam!: Team;
  submitted = false;
  message = '';

  constructor(
    private _route: ActivatedRoute,
    private _taskForm: FormBuilder,
    private _teamService: TeamService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    const teamId = +this._route.snapshot.params['id'];
    this.GetFormEditTeam(teamId);
  }

  private GetFormEditTeam(id: number) {
    this._teamService.GetTeam(id).subscribe((resp) => {
      if (resp.body != null) this.team = this.oldTeam = resp.body;
      this.SetValueForm();
    });
  }

  public SetValueForm() {
    const format = new FormatDatePipe();

    this.teamForm.setValue({
      id: this.oldTeam.id,
      name: this.oldTeam.name,
      createdAt: format.transform(this.oldTeam.createdAt),
    });
  }

  public SaveAndUpdate() {
    this.submitted = true;
    this.team = this.teamForm.value;

    console.log(this.teamForm.value);

    this.oldTeam.name = this.team.name;

    this._teamService.UpdateTeam(this.oldTeam.id, this.oldTeam).subscribe(
      (response) => {
        console.log(response);
        if (response.status == 200) this._router.navigate(['teams']);
      },
      (error) => {
        this.message = error;
        console.log(error);
      }
    );
  }

  public Reset() {
    this.submitted = false;
    this.teamForm.reset();
    this.SetValueForm();
  }
}
