import { UserService } from './../../services/user.service';
import { User } from './../../models/user';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  displayedColumns: string[] = [
    'No. user',
    'No. team',
    'First name',
    'Last name',
    'Email',
    'Registered at',
    'Birthday',
    'Update',
    'Delete',
  ];

  dataSource = new MatTableDataSource<User>();

  @ViewChild(MatPaginator) set paginator(p: MatPaginator) {
    this.dataSource.paginator = p;
  }

  constructor(private _userService: UserService, private _router: Router) {}

  ngOnInit(): void {
    this.GetUsers();
    this.dataSource.paginator = this.paginator;
  }

  private GetUsers() {
    this._userService.GetUsers().subscribe((resp) => {
      if (resp.body != null && resp.status == 200) {
        this.users = this.dataSource.data = resp.body;
      }
    });
  }

  public redirectToUpdate(id: number) {
    this._router.navigate(['edit-user/', id]);
  }

  public redirectToDelete(user: User) {
    var res = confirm('Are you sure you want to delete?');

    if (user && res) {
      this._userService.DeleteUser(user.id).subscribe((resp) => {
        if (resp.status == 200) {
          const indexDelete = this.users.indexOf(user);
          this.users.splice(indexDelete, 1);

          this.dataSource.data = [...this.users];
        }
      });
    }
  }
}
