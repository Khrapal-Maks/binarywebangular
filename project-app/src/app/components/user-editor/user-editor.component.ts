import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { FormatDatePipe } from 'src/app/pipe/format-date.pipe';

@Component({
  selector: 'app-user-editor',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.css'],
})
export class UserEditorComponent implements OnInit {
  userForm = this._taskForm.group({
    id: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    teamId: new FormControl(''),
    firstName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(100),
    ]),
    email: new FormControl('', [Validators.required]),
    registeredAt: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    birthDay: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
  });

  user!: User;
  oldUser!: User;
  submitted = false;
  message = '';

  constructor(
    private _route: ActivatedRoute,
    private _taskForm: FormBuilder,
    private _userService: UserService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    const userId = +this._route.snapshot.params['id'];
    this.GetFormEditUser(userId);
  }

  private GetFormEditUser(id: number) {
    this._userService.GetUser(id).subscribe((resp) => {
      if (resp.body != null) this.user = this.oldUser = resp.body;
      this.SetValueForm();
    });
  }

  public SetValueForm() {
    const format = new FormatDatePipe();

    this.userForm.setValue({
      id: this.oldUser.id,
      teamId: this.oldUser.teamId,
      firstName: this.oldUser.firstName,
      lastName: this.oldUser.lastName,
      email: this.oldUser.email,
      registeredAt: format.transform(this.oldUser.registeredAt),
      birthDay: format.transform(this.oldUser.birthDay),
    });
  }

  public SaveAndUpdate() {
    this.submitted = true;
    this.user = this.userForm.value;

    console.log(this.userForm.value);

    this.oldUser.teamId = this.user.teamId;
    this.oldUser.firstName = this.user.firstName;
    this.oldUser.lastName = this.user.lastName;
    this.oldUser.email = this.user.email;

    this._userService.UpdateUser(this.oldUser.id, this.oldUser).subscribe(
      (response) => {
        console.log(response);
        if (response.status == 200) this._router.navigate(['users']);
      },
      (error) => {
        this.message = error;
        console.log(error);
      }
    );
  }

  public Reset() {
    this.submitted = false;
    this.userForm.reset();
    this.SetValueForm();
  }
}
