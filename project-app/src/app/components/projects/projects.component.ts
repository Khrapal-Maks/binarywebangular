import { Project } from './../../models/project';
import { ProjectService } from './../../services/project.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
})
export class ProjectsComponent implements OnInit {
  projects: Project[] = [];
  displayedColumns: string[] = [
    'No. project',
    'No. currator',
    'No. team',
    'Name project',
    'Description project',
    'Created at',
    'Deadline',
    'Update',
    'Delete',
  ];

  dataSource = new MatTableDataSource<Project>();

  @ViewChild(MatPaginator) set paginator(p: MatPaginator) {
    this.dataSource.paginator = p;
  }

  constructor(
    private _projectService: ProjectService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.GetProjects();
    this.dataSource.paginator = this.paginator;
  }

  private GetProjects() {
    this._projectService.GetProjects().subscribe((resp) => {
      if (resp.body != null && resp.status == 200) {
        this.projects = this.dataSource.data = resp.body;
      }
    });
  }

  public redirectToUpdate(id: number) {
    this._router.navigate(['edit-project/', id]);
  }

  public redirectToDelete(project: Project) {
    var res = confirm('Are you sure you want to delete?');

    if (project && res) {
      this._projectService.DeleteProject(project.id).subscribe((resp) => {
        if (resp.status == 200) {
          const indexDelete = this.projects.indexOf(project);
          this.projects.splice(indexDelete, 1);

          this.dataSource.data = [...this.projects];
        }
      });
    }
  }
}
