import { Tasks } from '../../models/tasks';
import { TaskService } from './../../services/task.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-tasks',
  templateUrl: './project-tasks.component.html',
  styleUrls: ['./project-tasks.component.css'],
})
export class ProjectTasksComponent implements OnInit {
  tasks: Tasks[] = [];
  displayedColumns: string[] = [
    'No. task',
    'No. project',
    'No. performer',
    'Name task',
    'Description task',
    'Task state',
    'Created at',
    'Finished at',
    'Update',
    'Delete',
  ];

  dataSource = new MatTableDataSource<Tasks>();

  @ViewChild(MatPaginator) set paginator(p: MatPaginator) {
    this.dataSource.paginator = p;
  }

  constructor(private _taskService: TaskService, private _router: Router) {}

  ngOnInit(): void {
    this.GetTasks();
    this.dataSource.paginator = this.paginator;
  }

  private GetTasks() {
    this._taskService.GetTasks().subscribe((resp) => {
      if (resp.body != null && resp.status == 200) {
        this.tasks = this.dataSource.data = resp.body;
      }
    });
  }

  public redirectToUpdate(id: number) {
    this._router.navigate(['edit-task/', id]);
  }

  public redirectToDelete(task: Tasks) {
    var res = confirm('Are you sure you want to delete?');

    if (task && res) {
      this._taskService.DeleteTask(task.id).subscribe((resp) => {
        if (resp.status == 200) {
          const indexDelete = this.tasks.indexOf(task);
          this.tasks.splice(indexDelete, 1);

          this.dataSource.data = [...this.tasks];
        }
      });
    }
  }
}
