import { TaskStateEnum } from './../../models/task-state';
import { Tasks } from './../../models/tasks';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { FormatDatePipe } from 'src/app/pipe/format-date.pipe';

@Component({
  selector: 'app-tasks-editor',
  templateUrl: './tasks-editor.component.html',
  styleUrls: ['./tasks-editor.component.css'],
})
export class TasksEditorComponent implements OnInit {
  taskForm = this._taskForm.group({
    id: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    projectId: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    performerId: new FormControl(0, [
      Validators.required,
      Validators.maxLength(20),
    ]),
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    description: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(100),
    ]),
    state: new FormControl('', Validators.required),
    createdAt: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    finishedAt: new FormControl(''),
  });

  task!: Tasks;
  oldTask!: Tasks;
  submitted = false;
  message = '';

  states: string[] = ['IsCreated', 'IsProgress', 'IsTesting', 'IsDone'];

  constructor(
    private _route: ActivatedRoute,
    private _taskForm: FormBuilder,
    private _taskService: TaskService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    const taskId = +this._route.snapshot.params['id'];
    this.GetFormEditTask(taskId);
  }

  private GetFormEditTask(id: number) {
    this._taskService.GetTask(id).subscribe((resp) => {
      if (resp.body != null) this.task = this.oldTask = resp.body;
      this.SetValueForm();
    });
  }

  public SetValueForm() {
    const format = new FormatDatePipe();
    const date = format.transform(this.oldTask.createdAt)!;

    this.taskForm.setValue({
      id: this.oldTask.id,
      projectId: this.oldTask.projectId,
      performerId: this.oldTask.performerId,
      name: this.oldTask.name,
      description: this.oldTask.description,
      state: this.oldTask.state,
      createdAt: date,
      finishedAt: this.oldTask.finishedAt,
    });
  }

  public SaveAndUpdate() {
    this.submitted = true;
    this.task = this.taskForm.value;

    console.log(this.taskForm.value);

    this.oldTask.performerId = this.task.performerId;
    this.oldTask.name = this.task.name;
    this.oldTask.description = this.task.description;
    this.oldTask.state = this.task.state;
    this.oldTask.state != TaskStateEnum.IsDone
      ? (this.oldTask.finishedAt = null)
      : Date;

    this._taskService.UpdateTask(this.oldTask.id, this.oldTask).subscribe(
      (response) => {
        console.log(response);
        if (response.status == 200) this._router.navigate(['project-tasks']);
      },
      (error) => {
        this.message = error;
        console.log(error);
      }
    );
  }

  public Reset() {
    this.submitted = false;
    this.taskForm.reset();
    this.SetValueForm();
  }
}
