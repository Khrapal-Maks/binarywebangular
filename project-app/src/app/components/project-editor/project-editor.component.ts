import { ProjectService } from './../../services/project.service';
import { Project } from './../../models/project';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormatDatePipe } from 'src/app/pipe/format-date.pipe';

@Component({
  selector: 'app-project-editor',
  templateUrl: './project-editor.component.html',
  styleUrls: ['./project-editor.component.css'],
})
export class ProjectEditorComponent implements OnInit {
  projectForm = this._taskForm.group({
    id: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    authorId: new FormControl('', Validators.required),
    teamId: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    createdAt: new FormControl(
      { value: '', disabled: true },
      Validators.compose([Validators.required])
    ),
    deadline: new FormControl('', Validators.required),
  });

  project!: Project;
  oldProject!: Project;
  submitted = false;
  message = '';
  deadline?: Date;

  constructor(
    private _route: ActivatedRoute,
    private _taskForm: FormBuilder,
    private _projectService: ProjectService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    const projectId = +this._route.snapshot.params['id'];
    this.GetFormEditTeam(projectId);
  }

  private GetFormEditTeam(id: number) {
    this._projectService.GetProject(id).subscribe((resp) => {
      if (resp.body != null) this.project = this.oldProject = resp.body;
      this.SetValueForm();
    });
  }

  public SetValueForm() {
    const format = new FormatDatePipe();

    this.projectForm.setValue({
      id: this.oldProject.id,
      authorId: this.oldProject.authorId,
      teamId: this.oldProject.teamId,
      name: this.oldProject.name,
      description: this.oldProject.description,
      createdAt: format.transform(this.oldProject.createdAt),
      deadline: format.transform(this.oldProject.deadline),
    });
  }

  public SaveAndUpdate() {
    this.submitted = true;
    this.project = this.projectForm.value;

    const deadline = new Date(this.project.deadline);
    
    this.oldProject.authorId =  this.project.authorId;
    this.oldProject.teamId = this.project.teamId;
    this.oldProject.name = this.project.name;
    this.oldProject.description = this.project.description;
    this.oldProject.deadline = deadline;

    
    console.log(this.oldProject);
    this._projectService
      .UpdateProject(this.oldProject.id, this.oldProject)
      .subscribe(
        (response) => {
          console.log(response);
          if (response.status == 200) this._router.navigate(['projects']);
        },
        (error) => {
          this.message = error;
          console.log(error);
        }
      );
  }

  public Reset() {
    this.submitted = false;
    this.projectForm.reset();
    this.SetValueForm();
  }
}
