import { ProjectEditorComponent } from './components/project-editor/project-editor.component';
import { TeamEditorComponent } from './components/team-editor/team-editor.component';
import { UserEditorComponent } from './components/user-editor/user-editor.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './components/main/main.component';
import { ProjectTasksComponent } from './components/project-tasks/project-tasks.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { TasksEditorComponent } from './components/tasks-editor/tasks-editor.component';
import { TeamsComponent } from './components/teams/teams.component';
import { UsersComponent } from './components/users/users.component';
import { ExitAboutGuard } from './guards/edit-guard';

const routes: Routes = [
  
  { path: '', component: MainComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'project-tasks', component: ProjectTasksComponent},
  { path: 'teams', component: TeamsComponent },
  { path: 'users', component: UsersComponent },
  { path: 'edit-task/:id', component: TasksEditorComponent },
  { path: 'edit-user/:id', component: UserEditorComponent },
  { path: 'edit-team/:id', component: TeamEditorComponent},
  { path: 'edit-project/:id', component: ProjectEditorComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
