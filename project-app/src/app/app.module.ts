import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './components/common/material-components.module';
import { ProjectTasksComponent } from './components/project-tasks/project-tasks.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { TeamsComponent } from './components/teams/teams.component';
import { UsersComponent } from './components/users/users.component';
import { MainComponent } from './components/main/main.component';
import { ProjectService } from './services/project.service';
import { TaskService } from './services/task.service';
import { TeamService } from './services/team.service';
import { UserService } from './services/user.service';
import { ReactiveFormsModule } from '@angular/forms';
import { TasksEditorComponent } from './components/tasks-editor/tasks-editor.component';
import { FormatDatePipe } from './pipe/format-date.pipe';
import { ExitAboutGuard } from './guards/edit-guard';
import { UserEditorComponent } from './components/user-editor/user-editor.component';
import { TeamEditorComponent } from './components/team-editor/team-editor.component';
import { ProjectEditorComponent } from './components/project-editor/project-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectTasksComponent,
    ProjectsComponent,
    TeamsComponent,
    UsersComponent,
    MainComponent,
    TasksEditorComponent,
    FormatDatePipe,
    UserEditorComponent,
    TeamEditorComponent,
    ProjectEditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ProjectService, TaskService, TeamService, UserService, ExitAboutGuard],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
