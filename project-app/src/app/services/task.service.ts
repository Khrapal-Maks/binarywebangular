import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tasks } from '../models/tasks';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  public baseUrl: string = environment.apiUrl + '/api/tasks';
  public headersHTTP = new HttpHeaders({
    'content-type': 'application/json',
  });

  constructor(private _http: HttpClient) {}

  public GetTasks(): Observable<HttpResponse<Tasks[]>> {
    return this._http.get<Tasks[]>(this.baseUrl, { observe: 'response', headers: this.headersHTTP });
  }

  public GetTask(id: number): Observable<HttpResponse<Tasks>> {
    return this._http.get<Tasks>(`${this.baseUrl}/` + id, { observe: 'response', headers: this.headersHTTP });
  }

  public UpdateTask(id: number, task: Tasks): Observable<HttpResponse<Tasks>> {
    return this._http.put<Tasks>(`${this.baseUrl}/` + id, task, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }

  public DeleteTask(id: number): Observable<HttpResponse<Tasks>> {
    return this._http.delete<Tasks>(this.baseUrl + '/' + id, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }
}
