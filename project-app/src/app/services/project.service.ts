import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Project } from '../models/project';

@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  public baseUrl: string = environment.apiUrl + '/api/projects';
  public headersHTTP = new HttpHeaders({
    'content-type': 'application/json',
  });

  constructor(private _http: HttpClient) {}

  public GetProjects(): Observable<HttpResponse<Project[]>> {
    return this._http.get<Project[]>(this.baseUrl, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }

  public GetProject(id: number): Observable<HttpResponse<Project>> {
    return this._http.get<Project>(`${this.baseUrl}/` + id, { observe: 'response', headers: this.headersHTTP });
  }

  public UpdateProject(id: number, project: Project): Observable<HttpResponse<Project>> {
    return this._http.put<Project>(`${this.baseUrl}/` + id, project, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }

  public DeleteProject(id: number): Observable<HttpResponse<Project>> {
    return this._http.delete<Project>(this.baseUrl + '/' + id, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }
}
