import { Team } from './../models/team';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public baseUrl: string = environment.apiUrl + '/api/team';
  public headersHTTP = new HttpHeaders({
    'content-type': 'application/json',
  });

  constructor(private _http: HttpClient) {}

  public GetTeams(): Observable<HttpResponse<Team[]>> {
    return this._http.get<Team[]>(this.baseUrl, { observe: 'response', headers: this.headersHTTP });
  }

  public GetTeam(id: number): Observable<HttpResponse<Team>> {
    return this._http.get<Team>(`${this.baseUrl}/` + id, { observe: 'response', headers: this.headersHTTP });
  }

  public UpdateTeam(id: number, team: Team): Observable<HttpResponse<Team>> {
    return this._http.put<Team>(`${this.baseUrl}/` + id, team, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }

  public DeleteTeam(id: number): Observable<HttpResponse<Team>> {
    return this._http.delete<Team>(this.baseUrl + '/' + id, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }
}
