import { User } from './../models/user';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public baseUrl: string = environment.apiUrl + '/api/users';
  public headersHTTP = new HttpHeaders({
    'content-type': 'application/json',
  });

  constructor(private _http: HttpClient) {}

  public GetUsers(): Observable<HttpResponse<User[]>> {
    return this._http.get<User[]>(this.baseUrl, { observe: 'response', headers: this.headersHTTP });
  }

  public GetUser(id: number): Observable<HttpResponse<User>> {
    return this._http.get<User>(`${this.baseUrl}/` + id, { observe: 'response', headers: this.headersHTTP });
  }

  public UpdateUser(id: number, user: User): Observable<HttpResponse<User>> {
    return this._http.put<User>(`${this.baseUrl}/` + id, user, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }

  public DeleteUser(id: number): Observable<HttpResponse<User>> {
    return this._http.delete<User>(this.baseUrl + '/' + id, {
      observe: 'response',
      headers: this.headersHTTP,
    });
  }
}
