import { TaskStateEnum } from '../models/task-state';
import { Pipe, PipeTransform } from '@angular/core';  
  
  @Pipe({
    name: 'enumToArray'
  })
  export class TaskEnumPipe implements PipeTransform {

    transform(value: string) : number {
      switch (value)
      {
        case "IsCreated": return 0;
        case "IsProgress": return 1;
        case "IsTesting": return 2;
        case "IsDone": return 3;
        default: return 0;
      }
    }
  }