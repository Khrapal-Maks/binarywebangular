import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'formatDatePipe',
})
export class FormatDatePipe implements PipeTransform {
  transform(value: Date) {
    var datePipe = new DatePipe('en-UA');
    let result = datePipe.transform(value, 'dd MMMM yyyy');
    return result;
  }
}
